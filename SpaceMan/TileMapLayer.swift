//
//  TileMapLayer.swift
//  SpaceMan
//
//  Created by Haci Kale on 25/05/15.
//  Copyright (c) 2015 Haci Kale. All rights reserved.
//

import SpriteKit

class TileMapLayer: SKNode
{
    
    let gridSize: CGSize
    let layerSize: CGSize
    
    let tileSize: CGSize
    var atlas: SKTextureAtlas?
    
    init(tileSize: CGSize, gridSize: CGSize, layerSize: CGSize? = nil)
    {
        self.tileSize=tileSize
        self.gridSize=gridSize
        
        if layerSize != nil
        {
            self.layerSize = layerSize!
        }
        else
        {
            self.layerSize = CGSize(width: tileSize.width * gridSize.width, height: tileSize.height * gridSize.height)
        }
        super.init()
        
    }
    convenience init(atlasName: String, tileSize: CGSize, tileCodes: [String])
    {
        
        self.init(tileSize: tileSize,
            gridSize: CGSize(width: countElements(tileCodes[0]),
                height: tileCodes.count))
        
        atlas = SKTextureAtlas(named: atlasName)
        for row in 0..<tileCodes.count
        {
            let line = tileCodes[row]
            for (col,code) in enumerate(line)
            {
                if let tile = nodeForCode(code)?
                {
                    tile.position = positionForRow(row, col: col)
                    addChild(tile)
                }
                
            }
        }
    }
    
    func positionForRow(row: Int, col: Int) -> CGPoint
    {
        //x er kollone gange med bred
        let x = CGFloat(col) * tileSize.width + tileSize.width/2
        let y = CGFloat(row) * tileSize.height + tileSize.height/2
        return CGPoint(x: x, y: layerSize.height-y)//layersize.height-y gør at man tegner baggrunden som på billedet
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    func nodeForCode(tileCode: Character) -> SKNode?
    {
        if atlas == nil
        {
            return nil;
        }
        var tile: SKNode?
        switch tileCode
        {
            
        case "." : return nil
        case "b" : tile = Ufo()
        case "p" : tile = Player()
            
        default : println("did not understand tilecode\(tileCode)")
        }
        //this check if the tile goes from SKNode to SKSpriteNode
        if let sprite = tile as? SKSpriteNode
        {
            //yes, it is a SKSpriteNode and now we are letting it use the blendMode
            sprite.blendMode = .Replace
        }
        return tile;
    }
    
    func isValidTileCoord(coord: CGPoint) -> Bool
    {
        return (coord.x >= 0 && coord.x <= gridSize.width && coord.y >= 0 && coord.y <= gridSize.height)
    }
    
    //convert from x,y to tile-coordinates
    func coordForPoint(point: CGPoint) -> CGPoint
    {
        return CGPoint(x: Int(point.x / tileSize.width),
            y: Int((point.y - layerSize.height) / -tileSize.height))
        
    }
    
    func pointForCoord(coord: CGPoint) -> CGPoint
    {
        
        return positionForRow(Int(coord.y), col: Int(coord.x))
        
    }
    
    func tileAtPoint(point: CGPoint) -> SKNode?
    {
        var node : SKNode? = nodeAtPoint(point)
        return node;
    }
    
    
}

//
//  Settings.swift
//  SpaceMan
//
//  Created by Haci Kale on 27/05/15.
//  Copyright (c) 2015 Haci Kale. All rights reserved.
//

import Foundation

private let _settingsInstance = Settings()

class Settings {
    var virtualPad = true
    
    class var sharedInstance: Settings {
        return _settingsInstance
    }
}
//
//  Player.swift
//  SpaceMan
//
//  Created by Haci Kale on 25/05/15.
//  Copyright (c) 2015 Haci Kale. All rights reserved.
//

import SpriteKit

class Player : SKNode
{
    var playerFacing: String!
    
    let sprite: SKSpriteNode!
    
    var idleDownTexture: SKTexture!
    var idleUpTexture: SKTexture!
    var idleLeftTexture: SKTexture!
    
    var walkLeftTexture1: SKTexture!
    var walkLeftTexture2: SKTexture!
    var walkLeftTexture3: SKTexture!
    var walkLeftTexture4: SKTexture!
    
    var walkUpTexture1: SKTexture!
    var walkUpTexture2: SKTexture!
    var walkUpTexture3: SKTexture!
    var walkUpTexture4: SKTexture!
    
    var walkDownTexture1: SKTexture!
    var walkDownTexture2: SKTexture!
    var walkDownTexture3: SKTexture!
    var walkDownTexture4: SKTexture!
    
    var walkSideWaysAnimation: SKAction!
    var walkUpAnimation: SKAction!
    var walkDownAnimation: SKAction!
    
    
    //added
    required init?(coder aDecoder: NSCoder)
    {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init()
    {
         super.init()
        createTextures()
        createAnimations()
    
        sprite = SKSpriteNode(texture: idleDownTexture)
        createPhysicsBody()
        
        addChild(sprite)
        name = "player"
        playerFacing = "Down"
        
    }
    
    func createPhysicsBody()
    {
        
        let smallerSize = CGSize(width: sprite.size.width/1.5, height: sprite.size.height/1.5)
       
        self.physicsBody = SKPhysicsBody(rectangleOfSize: smallerSize)
        
        self.physicsBody?.allowsRotation = false //not rotate
        self.physicsBody?.restitution = 1 // 0.1 loses 90% of the speed after colliding
        self.physicsBody?.friction = 0 // will lose no speed, when interacting with other objects
        self.physicsBody?.linearDamping = 0.4 // will lose speed, travelling through air/water/fluent
        self.physicsBody?.categoryBitMask = PhysicsCategory.Player
        self.physicsBody?.contactTestBitMask = PhysicsCategory.All
        self.physicsBody?.collisionBitMask = PhysicsCategory.Border | PhysicsCategory.Wall | PhysicsCategory.Water
    
    }
    
    func createTextures()
    {
        
        walkLeftTexture1 = SKTexture(imageNamed: "player_lt1.png")
        walkLeftTexture2 = SKTexture(imageNamed: "player_lt2.png")
        walkLeftTexture3 = SKTexture(imageNamed: "player_lt3.png")
        walkLeftTexture4 = SKTexture(imageNamed: "player_lt4.png")
        
        walkUpTexture1 = SKTexture(imageNamed: "player_bk1.png")
        walkUpTexture2 = SKTexture(imageNamed: "player_bk2.png")
        walkUpTexture3 = SKTexture(imageNamed: "player_bk3.png")
        walkUpTexture4 = SKTexture(imageNamed: "player_bk4.png")
        
        walkDownTexture1 = SKTexture(imageNamed: "player_ft1.png")
        walkDownTexture2 = SKTexture(imageNamed: "player_ft2.png")
        walkDownTexture3 = SKTexture(imageNamed: "player_ft3.png")
        walkDownTexture4 = SKTexture(imageNamed: "player_ft4.png")
        
        //for more readable code I create a reference to the texture I want to use for idle
        idleDownTexture = walkDownTexture1
        idleUpTexture = walkUpTexture1
        idleLeftTexture = walkLeftTexture1
        
    }
    
    func createAnimations()
    {
        var walkSidewaysAnimationAction = SKAction.animateWithTextures([ walkLeftTexture1, walkLeftTexture2, walkLeftTexture3,walkLeftTexture4], timePerFrame: 0.1)
        walkSideWaysAnimation = SKAction.repeatActionForever(walkSidewaysAnimationAction)
        
        var walkDownAnimationAction = SKAction.animateWithTextures([ walkDownTexture1, walkDownTexture2, walkDownTexture3,walkDownTexture4], timePerFrame: 0.1)
        walkDownAnimation = SKAction.repeatActionForever(walkDownAnimationAction)
        
        var walkUpAnimationAction = SKAction.animateWithTextures([ walkUpTexture1, walkUpTexture2, walkUpTexture3,walkUpTexture4], timePerFrame: 0.1)
        walkUpAnimation = SKAction.repeatActionForever(walkUpAnimationAction)

    }
    
    func stand()
    {
        sprite.removeAllActions()
        
        switch(playerFacing)
        {
        case "Left":
            sprite.texture = idleLeftTexture
            sprite.xScale = 1
        case "Right":
            sprite.texture = idleLeftTexture
            sprite.xScale = -1
        case "Down":
            sprite.texture = idleDownTexture
            sprite.xScale = 1
        case "Up":
            sprite.texture = idleUpTexture
            sprite.xScale = 1
        default:
        sprite.texture = idleDownTexture
        }
    }
    
    func walkLeft()
    {
        playerFacing = "Left"
        sprite.runAction(walkSideWaysAnimation)
        sprite.xScale = 1

    }
    
    func walkRight()
    {
        playerFacing = "Right"
        sprite.runAction(walkSideWaysAnimation)
        sprite.xScale = -1
        
    }
    
    func walkDown()
    {
        playerFacing = "Down"
        sprite.runAction(walkDownAnimation)
        sprite.xScale = 1
    }
    
    func walkUp()
    {
        playerFacing = "Up"
        sprite.runAction(walkUpAnimation)
        sprite.xScale = 1
    }
    
}
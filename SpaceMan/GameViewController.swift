//
//  GameViewController.swift
//  SpaceMan
//
//  Created by Haci Kale on 25/05/15.
//  Copyright (c) 2015 Haci Kale. All rights reserved.
//

import UIKit
import SpriteKit

extension SKNode {
    class func unarchiveFromFile(file : NSString) -> SKNode? {
        if let path = NSBundle.mainBundle().pathForResource(file, ofType: "sks") {
            var sceneData = NSData(contentsOfFile: path, options: .DataReadingMappedIfSafe, error: nil)!
            var archiver = NSKeyedUnarchiver(forReadingWithData: sceneData)
            
            archiver.setClass(self.classForKeyedUnarchiver(), forClassName: "SKScene")
            let scene = archiver.decodeObjectForKey(NSKeyedArchiveRootObjectKey) as GameScene
            archiver.finishDecoding()
            return scene
        } else {
            return nil
        }
    }
}

class GameViewController: UIViewController
{
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        //            let scene = GameScene(size:CGSize(width: 1024, height: 768))
        let scene = GameScene(size: view.bounds.size)
        let skView = self.view as SKView
        skView.showsFPS = true
        skView.showsNodeCount = true
        skView.ignoresSiblingOrder = true
        scene.scaleMode = .ResizeFill
        skView.presentScene(scene)
        skView.showsPhysics = false
        
    }
    
}

func shouldAutorotate() -> Bool
{
    return true
}

func supportedInterfaceOrientations() -> Int
{
    if UIDevice.currentDevice().userInterfaceIdiom == .Phone
    {
        return Int(UIInterfaceOrientationMask.AllButUpsideDown.rawValue)
    } else
    {
        return Int(UIInterfaceOrientationMask.All.rawValue)
    }
}

func didReceiveMemoryWarning()
{
    // super.didReceiveMemoryWarning()
    // Release any cached data, images, etc that aren't in use.
}

func prefersStatusBarHidden() -> Bool {
    return true
}

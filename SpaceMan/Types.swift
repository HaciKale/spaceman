//
//  Types.swift
//  SpaceMan
//
//  Created by Haci Kale on 27/05/15.
//  Copyright (c) 2015 Haci Kale. All rights reserved.
//


enum GameState
{
    case StartingLevel
    case Playing
    case EndTheLevel
}
struct PhysicsCategory {
    
    static let None : UInt32 = 0b00000000
    static let Player : UInt32 = 0b00000001
    static let Ufo : UInt32 = 0b00000010
    static let Border : UInt32 = 0b00000100
    static let Water : UInt32 = 0b00001000
    static let Wall : UInt32 = 0b00010000
    static let Edge : UInt32 = 0b00100000
    static let Fire : UInt32 = 0b01000000
    static let All : UInt32 = 0b11111111
}


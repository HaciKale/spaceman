//
//  ControllerButton.swift
//  SpaceMan
//
//  Created by Haci Kale on 26/05/15.
//  Copyright (c) 2015 Haci Kale. All rights reserved.
//

//class ControllerButton: SKSpriteNode
//{
//    var hitbox: (CGPoint -> Bool)?
//    // [..]
//    
//    // This is an initializer, it is called at the creation of the object
//    // We can move some code here so we don't have to write it several times
//    convenience init(imageNamed: String, position: CGPoint)
//    {
//        self.init(imageNamed: imageNamed)
//        self.texture?.filteringMode = .Nearest
//        self.setScale(0.8)
//        self.alpha = 0.9
//        self.zPosition = 50
//        self.position = position
//        
//        hitbox = { (location: CGPoint) -> Bool in
//            return self.containsPoint(location)
//        }
//    }
//    
//    func hitboxContainsPoint(location: CGPoint) -> Bool {
//        return hitbox!(location)
//    }
//}


class ControllerButton: SKSpriteNode {
    var hitbox: (CGPoint -> Bool)?
      
    // This is an initializer, it is called at the creation of the object
    // We can move some code here so we don't have to write it several times
    // We change the initializer to set the new property
    convenience init(imageNamed: String, position: CGPoint) {
        // ..
        self.init(imageNamed: imageNamed)
        self.texture?.filteringMode = .Nearest
        self.setScale(0.8)
        self.alpha = 0.9
        self.zPosition = 50
        self.position = position
        // We initialize the property
      //  self.value = value
        
        hitbox = { (location: CGPoint) -> Bool in
            return self.containsPoint(location)
        }
    }
    
    
}

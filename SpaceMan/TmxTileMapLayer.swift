//class TmxTileMapLayer : TileMapLayer
//{
//    var layer: TMXLayer
//   
//    required init?(coder aDecoder: NSCoder)
//    {
//        fatalError("NSCoding not supported")
//    }
//    init(tmxLayer: TMXLayer)
//    {
//        layer = tmxLayer
//        super.init(tileSize: tmxLayer.mapTileSize,
//        gridSize: tmxLayer.layerInfo.layerGridSize, layerSize: CGSize(width: tmxLayer.layerWidth,
//        height: tmxLayer.layerHeight))
//    }
//}

class TmxTileMapLayer : TileMapLayer
{
    var layer: TMXLayer
    
    required init?(coder aDecoder: NSCoder)
    {
        fatalError("NSCoding not supported")
    }
    
    init(tmxLayer: TMXLayer)
    {
        layer = tmxLayer
        super.init(tileSize: tmxLayer.mapTileSize,
            gridSize: tmxLayer.layerInfo.layerGridSize,
            layerSize: CGSize(width: tmxLayer.layerWidth,
                height: tmxLayer.layerHeight))
        createNodesFromLayer(tmxLayer)
        
    }
    
    func createNodesFromLayer(layer: TMXLayer)
    {
        let map = layer.map
        for w in 0..<Int(gridSize.width)
        {
            for h in 0..<Int(gridSize.height)
            {
                let coord = CGPoint(x: w, y: h)
                let tileGid = layer.layerInfo.tileGidAtCoord(coord)
                
                if tileGid == 0
                {
                    continue
                }
                
                if let properties = map.propertiesForGid(tileGid)
                {
                    if properties["wall"] != nil
                    {
                        //we haven't learned to smash through a wall yet
                        let tile = layer.tileAtCoord(coord)
                        tile.physicsBody = SKPhysicsBody(rectangleOfSize: tile.size)
                        tile.physicsBody?.categoryBitMask = PhysicsCategory.Wall
                        tile.physicsBody?.friction = 0
                        tile.physicsBody?.dynamic = false
                    }
                    if properties["water"] != nil
                    {
                        //we can't swim yet
                        let tile = layer.tileAtCoord(coord)
                        tile.physicsBody = SKPhysicsBody(rectangleOfSize: tile.size)
                        tile.physicsBody?.categoryBitMask = PhysicsCategory.Water
                        tile.physicsBody?.friction = 0
                        tile.physicsBody?.dynamic = false
                    }
                    if properties["edges"] != nil
                    {
                        //we can't swim yet
                        let tile = layer.tileAtCoord(coord)
                        tile.physicsBody = SKPhysicsBody(rectangleOfSize: tile.size)
                        tile.physicsBody?.categoryBitMask = PhysicsCategory.Edge
                        tile.physicsBody?.friction = 0
                        tile.physicsBody?.dynamic = false
                    }


                }
            }
        }
    }
    
    override func tileAtPoint(point: CGPoint) -> SKNode?
    {
        let tile = super.tileAtPoint(point)
        return tile ?? layer.tileAt(point)//is short hand for: return tile == nil? layer.tileAt(point):tile
    }
}
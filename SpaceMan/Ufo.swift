//
//  Ufo.swift
//  SpaceMan
//
//  Created by Haci Kale on 27/05/15.
//  Copyright (c) 2015 Haci Kale. All rights reserved.
//

import SpriteKit

class Ufo : SKNode
{
    
    let sprite : SKSpriteNode
    
    var ufoTexture0: SKTexture!
    var ufoTexture1: SKTexture!
    var ufoTexture2: SKTexture!
    var ufoTexture3: SKTexture!
    var ufoTexture4: SKTexture!
    var ufoTexture5: SKTexture!
    var ufoTexture6: SKTexture!
    var ufoTexture7: SKTexture!
    var ufoTexture8: SKTexture!
    var ufoTexture9: SKTexture!
    var ufoTexture10: SKTexture!
    var ufoTexture11: SKTexture!
    
    var ufoAnimation: SKAction!
    
    
    //added
    required init?(coder aDecoder: NSCoder)
    {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init()
    {
        
        let texture = SKTexture(imageNamed: "ufo_0.png")
        sprite = SKSpriteNode(texture: texture)
        
        super.init()
        
        createTextures()
        createAnimations()
       
        sprite.setScale(0.8)
         createPhysicsBody()
        
        addChild(sprite)
        name = "ufo"
        
    }
    
    func walk()
    {
        let tileLayer = parent as? TileMapLayer
        let tileCoord = tileLayer?.coordForPoint(position)
        
        //get random value : -1,0,1
        let randomX = CGFloat(Int.random(min: -1, max: 1))
        let randomY = CGFloat(Int.random(min: -1, max: 1))
        let randomCoord = CGPoint(x: tileCoord!.x + randomX, y: tileCoord!.y + randomY)
        
        if tileLayer!.isValidTileCoord(randomCoord)
        {
            let randomPos = tileLayer?.pointForCoord(randomCoord)
            let moveToPos = SKAction.sequence([SKAction.moveTo(randomPos!, duration: 1),SKAction.runBlock(walk)])
            runAction(moveToPos)
        }
        else
        {
            let pause = SKAction.waitForDuration(0.25, withRange: 0.15)
            let retry = SKAction.runBlock(walk)
            runAction(SKAction.sequence([pause, retry]))
        }
    }
    
    func createTextures()
    {
        
          ufoTexture0 = SKTexture(imageNamed: "ufo_0.png")
          ufoTexture1 = SKTexture(imageNamed: "ufo_1.png")
          ufoTexture2 = SKTexture(imageNamed: "ufo_2.png")
          ufoTexture3 = SKTexture(imageNamed: "ufo_3.png")
          ufoTexture4 = SKTexture(imageNamed: "ufo_4.png")
          ufoTexture5 = SKTexture(imageNamed: "ufo_5.png")
          ufoTexture6 = SKTexture(imageNamed: "ufo_6.png")
          ufoTexture7 = SKTexture(imageNamed: "ufo_7.png")
          ufoTexture8 = SKTexture(imageNamed: "ufo_8.png")
          ufoTexture9 = SKTexture(imageNamed: "ufo_9.png")
          ufoTexture10 = SKTexture(imageNamed: "ufo_10.png")
          ufoTexture11 = SKTexture(imageNamed: "ufo_11.png")
        
    }
    
    func createAnimations()
    {
        var ufoAnimationAction = SKAction.animateWithTextures([ufoTexture1,ufoTexture2,ufoTexture3,ufoTexture4,ufoTexture5,ufoTexture6,ufoTexture7,ufoTexture8,ufoTexture9,ufoTexture10,ufoTexture11], timePerFrame: 0.1)
        ufoAnimation = SKAction.repeatActionForever(ufoAnimationAction)
    }
    
    func animate()
    {
        sprite.runAction(ufoAnimation)
    }
    func createPhysicsBody()
    {
        
        let radius = min(sprite.size.width, sprite.size.height)/2
        
        self.physicsBody = SKPhysicsBody(circleOfRadius: radius)
        
        self.physicsBody?.categoryBitMask = PhysicsCategory.Ufo
        self.physicsBody?.collisionBitMask = PhysicsCategory.Player | PhysicsCategory.Border | PhysicsCategory.Wall | PhysicsCategory.Water
        self.physicsBody?.contactTestBitMask = PhysicsCategory.All
        
    }


    
}
//
//  GameScene.swift
//  PestControl
//
//  Created by Haci Kale on 13/03/15.
//  Copyright (c) 2015 Haci Kale. All rights reserved.
//

import SpriteKit


class GameScene: SKScene, SKPhysicsContactDelegate
{
    var worldNode: SKNode!
    var backgroundLayer: TileMapLayer!
    var bgLayer: TileMapLayer!
    var ufoLayer: TileMapLayer!
    var player : Player!
    private var ufosToRemove: [Ufo] = []
    var tileMap: JSTileMap?
    var scoreLabel = SKLabelNode(fontNamed: "AlienMushrooms")
    var score=0
    var gameState = GameState.StartingLevel
    var startMsg = SKLabelNode(fontNamed: "AlienMushrooms")
   
    
    let buttonDirUp = ControllerButton(imageNamed: "flatDark02",position: CGPoint(x: -220, y: -70))
    let buttonDirDown = ControllerButton(imageNamed: "flatDark09",position: CGPoint(x: -220, y: -130))
    let buttonDirRight = ControllerButton(imageNamed: "flatDark05",position: CGPoint(x: -190, y: -100))
    let buttonDirLeft = ControllerButton(imageNamed: "flatDark04",position: CGPoint(x: -251, y: -100))
    let buttonA = ControllerButton(imageNamed: "flatDark47",position: CGPoint(x: 210, y: -100))
    
    // we define and initialize an empty list of pressed buttons.
    var pressedButtons = [ControllerButton]()// This is how to initialize an empty array of SKSpriteNode
    
    
    override func didMoveToView(view: SKView)
    {
        self.physicsWorld.gravity = CGVector.zeroVector
        physicsWorld.contactDelegate = self
        createWorld()
        createCharacters()
        createScoreLabel()
        centerViewOn(CGPoint(x: 400, y: 400))
        createUserInterface()
        
        
        self.addChild(buttonDirUp)
        self.addChild(buttonDirDown)
        self.addChild(buttonDirRight)
        self.addChild(buttonDirLeft)
        self.addChild(buttonA)
        
        if gameState == .StartingLevel
        {
            paused = true
        }
        
    }
    
    //creates a sceneary by Loading the Tmx file named Level-1 and the layernamed bg
    func createScenery()->TileMapLayer?
    {
        tileMap = JSTileMap(named: "level-1.tmx")
        tileMap?.zPosition = -2
        return TmxTileMapLayer(tmxLayer: tileMap!.layerNamed("bg"))
    }
    
    func createScenery2()->TileMapLayer?
    {
        tileMap = JSTileMap(named: "level-1.tmx")
        tileMap?.zPosition = -1
        return TmxTileMapLayer(tmxLayer: tileMap!.layerNamed("sbg"))
    }
    
    
    func createScoreLabel()
    {
    scoreLabel.fontSize=36
    scoreLabel.position=CGPoint(x: -170, y: 140)
    scoreLabel.text="SCORE:\(score)"
    scoreLabel.zPosition=140
        addChild(scoreLabel)
    }
    
    func createUserInterface()
    {
        startMsg.name = "startGame"
        startMsg.text = "TAP IF YOU WANNA PLAY!"
        startMsg.fontSize = 40
        startMsg.position = CGPoint(x: 0, y: 20)
        startMsg.zPosition = 100
        addChild(startMsg)
        
        
    }
    func createWorld()
    {
        worldNode = SKNode()
        backgroundLayer = createScenery()
        bgLayer = createScenery2()
        
        if tileMap != nil
        {
            worldNode.addChild(tileMap!)
        }
        worldNode.addChild(bgLayer)
        worldNode.addChild(backgroundLayer)
        addChild(worldNode)
        
        anchorPoint = CGPoint(x: 0.5, y: 0.5)
        worldNode.position = CGPoint(x: -backgroundLayer.layerSize.width/2, y: -backgroundLayer.layerSize.height/2)
        
        //create the box, which surrounds the background layer
        let bounds = SKNode()
        bounds.physicsBody = SKPhysicsBody (edgeLoopFromRect: CGRect(x: 0, y: 0, width: backgroundLayer.layerSize.width , height: backgroundLayer.layerSize.height))
        bounds.physicsBody?.friction = 0 // sprites will not loose speed after contact
        worldNode.addChild(bounds)
    }
    
    
    func getCenterPointWithTarget(centerOn: CGPoint) -> CGPoint
    {
        //worldNode.Position = ??
        let x = centerOn.x.clamped(size.width/2, backgroundLayer.layerSize.width - size.width/2)
        let y = centerOn.y.clamped(size.height/2, backgroundLayer.layerSize.height - size.height/2)
        return CGPoint(x: -x, y: -y)
    }
    
    func centerViewOn(centerOn: CGPoint)
    {
        worldNode.position = getCenterPointWithTarget(centerOn)
    }

    func endLevelWithSuccess(won: Bool)
    { // 1
        let label = childNodeWithName("startGame") as SKLabelNode
        label.text = won ? "GOOD JOB!" : "OH NO!"
        label.hidden = false
        // 2
        let retryLevel = SKLabelNode(fontNamed: "AlienMushrooms")
        retryLevel.text = "PLAY AGAIN?"
        retryLevel.name = "retryLbl"
        retryLevel.fontSize = 28
        retryLevel.horizontalAlignmentMode = won ? .Center : .Left
        retryLevel.position = won ? CGPoint(x: 0, y: -40) : CGPoint(x: 20, y: -40)
        retryLevel.zPosition = 100
        addChild(retryLevel)
        // 3
        player.physicsBody!.linearDamping = 1
        gameState = .EndTheLevel
        
    }
    override func didSimulatePhysics()
    {
        //centerViewOn(player.position)
        let target = getCenterPointWithTarget(player.position)
        worldNode.position += (target - worldNode.position) * 0.1
        // += dvs tag hver der er = og + det til f.eks. worldnode.position
        // * 0.1 (this will center the map 10% closer to the player for each frame)
        if !ufosToRemove.isEmpty { for ufo in ufosToRemove {
            ufo.removeFromParent() }
            ufosToRemove.removeAll() }
    }
    
    override func touchesBegan(touches: NSSet, withEvent event: UIEvent)
    {
        
        switch(gameState)
            {
        case .EndTheLevel:
            gameState = .StartingLevel
            
        case .StartingLevel:
            childNodeWithName("startGame")!.hidden = true
            gameState = .Playing
            paused = false
        
            fallthrough
        case .Playing:
        // if I press a button, I want to add it to the pressed buttons list
        for touch: AnyObject in touches {
            let location = touch.locationInNode(self)
            
            // for all 5 buttons
            for button in [buttonDirUp, buttonDirLeft, buttonDirDown, buttonDirRight,buttonA]
            {
                // I check if they are already registered in the list
                if button.containsPoint(location) && find(pressedButtons, button) == nil
                {
                    pressedButtons.append(button)
                    
                    
                    if find(pressedButtons, buttonDirUp) != nil {
                        player.walkUp()
                    }
                    if find(pressedButtons, buttonDirDown) != nil {
                        player.walkDown()
                    }
                    if find(pressedButtons, buttonDirLeft) != nil {
                       player.walkLeft()
                    }
                    if find(pressedButtons, buttonDirRight) != nil {
                       player.walkRight()
                    }
                    }
                }
            }
        }
        // then I check all the 5 buttons and set the transparency according if they are in the list or not
        for button in [buttonDirUp, buttonDirLeft, buttonDirDown, buttonDirRight,buttonA]
        {
            if find(pressedButtons, button) == nil
            {
                button.alpha = 0.9
            }
            else {
                button.alpha = 1
            }
        }
    }
    
    override func touchesMoved(touches: NSSet, withEvent event: UIEvent) {
        // if I move off a button, I remove it from the list, if I move on a button, I add it to the list
        for touch: AnyObject in touches {
            let location = touch.locationInNode(self)
            let previousLocation = touch.previousLocationInNode(self)
            
            for button in [buttonDirUp, buttonDirLeft, buttonDirDown, buttonDirRight,buttonA] {
                // if I get off the button where my finger was before
                if button.containsPoint(previousLocation) && !button.containsPoint(location) {
                    // I remove it from the list
                    let index = find(pressedButtons, button)
                    if index != nil {
                        pressedButtons.removeAtIndex(index!)
                    }
                }
                    // if I get on the button where I wasn't previously
                else if !button.containsPoint(previousLocation) && button.containsPoint(location) && find(pressedButtons, button) == nil {
                    // I add it to the list
                    pressedButtons.append(button)
                    

                }
            }
        }
        // update transparency for all 5 buttons
        for button in [buttonDirUp, buttonDirLeft, buttonDirDown, buttonDirRight,buttonA] {
            if find(pressedButtons, button) == nil {
                button.alpha = 0.9
            }
            else {
                button.alpha = 1
            }
        }
    }
    
    override func touchesEnded(touches: NSSet, withEvent event: UIEvent) {
        touchesEndedOrCancelled(touches, withEvent: event)
    }
    
    //if the system overrides your touches then this method goes..
    override func touchesCancelled(touches: NSSet, withEvent event: UIEvent) {
        touchesEndedOrCancelled(touches, withEvent: event)
    }
    
    func touchesEndedOrCancelled(touches: NSSet, withEvent event: UIEvent) {
        
        // when a touch is ended, remove it from the list
        for touch: AnyObject in touches
        {
        
            let location = touch.locationInNode(self)
            let previousLocation = touch.previousLocationInNode(self)
            
            for button in [buttonDirUp, buttonDirLeft, buttonDirDown, buttonDirRight,buttonA]
            {
                if button.containsPoint(location)
                {
                    let index = find(pressedButtons, button)
                    if index != nil
                    {
                        self.player.sprite.removeAllActions()
                        pressedButtons.removeAtIndex(index!)
                        
                        
                    }
                }
                else if (button.containsPoint(previousLocation))
                {
                    let index = find(pressedButtons, button)
                    if index != nil
                    {
                        pressedButtons.removeAtIndex(index!)
                    }
                }
            }
        }
        for button in [buttonDirUp, buttonDirLeft, buttonDirDown, buttonDirRight,buttonA] {
            if find(pressedButtons, button) == nil {
                button.alpha = 0.9
            }
            else {
                button.alpha = 1
            }
        }
    }

    
        func didBeginContact(contact:  SKPhysicsContact)
        {
            let contactMask = contact.bodyA.categoryBitMask | contact.bodyB.categoryBitMask
            
            switch(contactMask) {
                
            case PhysicsCategory.Ufo | PhysicsCategory.Player :
                score++
                scoreLabel.text="SCORE: \(score)"
                ufosToRemove.append(contact.bodyB.node as Ufo)
                
                
            default:
                break;
            }
        }
        
   

    func createCharacters()
    {
        //creating Ufo
        ufoLayer = tileMapLayerFromFileNamed("level-1-ufo.txt")
        ufoLayer.zPosition = 10
        worldNode.addChild(ufoLayer)
        
        //creating player by loading the map, finding the "player"
        player = ufoLayer.childNodeWithName("player") as Player
        player.removeFromParent() //removing him from buglayer
        player.zPosition = 50 //his zposition is the 50 so he is always on top of everything
        worldNode.addChild(player) //and then we add him to the worldnode
        
        
        ufoLayer.enumerateChildNodesWithName("ufo", usingBlock: { node, _ in
            if let ufo = node as? Ufo
            {
                ufo.walk()
                ufo.animate()
            }
        })
    }
    

  
    override func update(currentTime: CFTimeInterval)
    {
        
        if pressedButtons.isEmpty
        {
            player.stand()
        }
        else
        {
            var walkingSpeed: CGFloat = 5.0
           
            
            //this check if 2 buttons are pressed it divided
            if pressedButtons.count == 2
            {
            walkingSpeed = walkingSpeed / sqrt(2.0)
            }
        
            if find(pressedButtons, buttonDirUp) != nil
            {
                player.position.y += walkingSpeed
            }
            if find(pressedButtons, buttonDirDown) != nil
            {
                player.position.y -= walkingSpeed
            }
            if find(pressedButtons, buttonDirLeft) != nil
            {
                player.position.x -= walkingSpeed
            }
            if find(pressedButtons, buttonDirRight) != nil
            {
                player.position.x += walkingSpeed
            }
            if find(pressedButtons, buttonA) != nil
            {
                println("Something")

            }
        }
                
    }


}

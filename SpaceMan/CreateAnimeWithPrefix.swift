//
//  CreateAnimeWithPrefix.swift
//  SpaceMan
//
//  Created by Haci Kale on 27/05/15.
//  Copyright (c) 2015 Haci Kale. All rights reserved.
//
class func createAnimWithPrefix(prefix: String,
    suffix: String) -> SKAction {
    let atlas = SKTextureAtlas(named: "characters")
    let textures = [atlas.textureNamed("\(prefix)_\(suffix)1"),
    atlas.textureNamed("\(prefix)_\(suffix)2")] textures[0].filteringMode = .Nearest
    textures[1].filteringMode = .Nearest
    return SKAction.repeatActionForever( SKAction.animateWithTextures(textures, timePerFrame:0.20))
}